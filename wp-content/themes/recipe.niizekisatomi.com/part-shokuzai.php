<?
  $is_index =  isset($args['is_index']) ? $args['is_index'] : false;
?>
<div class="c-shokuzai-tags">
  <ul class="e-list">
    <?
      $terms = get_terms('recipe_ingredients', [
        'orderby' => 'count',
        'order' => 'desc',
      ]);

      foreach($terms as $term):
    ?>
    <li class="e-item">
      <? if ($is_index): ?>
        <a class="e-link" href="<?=home_url('/recipes?recipe_ingredients='.$term->slug)?>"><?= $term->name ?></a>
      <? else: ?>
        <button class="e-link" data-term="recipe_ingredients=<?= urldecode($term->slug)?>"><?= $term->name ?></button>
      <? endif; ?>
    </li>
    <? endforeach;?>
  </ul>
  <button class="e-open-button"></button>
</div>