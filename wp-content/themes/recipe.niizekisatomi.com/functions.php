<?php /* このファイルはsrcからではなく直接編集*/


/////////// ブロックに対応 ////////////////
// Add support for Block Styles.
add_theme_support( 'wp-block-styles' );

/////////// レシピのカスタム投稿タイプとカスタムタクソノミー ////////////////
add_action( 'init', 'create_post_type' );
function create_post_type() {
  register_post_type( 'recipes', [ // 投稿タイプ名の定義
      'labels' => [
          'name'          => 'レシピ', // 管理画面上で表示する投稿タイプ名
      ],
      'public'        => true,  // 投稿タイプをpublicにするか
      'has_archive'   => true, // アーカイブ機能ON/OFF
      'menu_position' => 5,     // 管理画面上での配置場所
      'show_in_rest'  => true,  // 5系から出てきた新エディタ「Gutenberg」を有効にする
      'rewrite' => ['with_front' => false],
      'supports' => ['title', 'editor', 'thumbnail']
  ]);

  $args = array(
    'label' => 'ジャンル', //
    'public' => true,
    'show_ui' => true,
    'show_in_nav_menus' => true,
    'show_admin_column' => true,
    'hierarchical' => true,
    'query_var' => true,
    'show_in_rest' => true,
  );
  register_taxonomy('recipe_genre','recipes',$args);

  $args = array(
    'label' => '季節', //
    'public' => true,
    'show_ui' => true,
    'show_in_nav_menus' => true,
    'show_admin_column' => true,
    'hierarchical' => true,
    'query_var' => true,
    'show_in_rest' => true,
  );
  register_taxonomy('recipe_season','recipes',$args);

  $args = array(
    'label' => '調味料', //
    'public' => true,
    'show_ui' => true,
    'show_in_nav_menus' => true,
    'show_admin_column' => true,
    'hierarchical' => true,
    'query_var' => true,
    'show_in_rest' => true,
  );
  register_taxonomy('recipe_condiment','recipes',$args);

  $args = array(
    'label' => 'つくり方', //
    'public' => true,
    'show_ui' => true,
    'show_in_nav_menus' => true,
    'show_admin_column' => true,
    'hierarchical' => true,
    'query_var' => true,
    'show_in_rest' => true,
  );
  register_taxonomy('recipe_method','recipes',$args);

  $args = array(
    'label' => '食材', //
    'public' => true,
    'show_ui' => true,
    'show_in_nav_menus' => true,
    'show_admin_column' => true,
    'hierarchical' => false,
    'query_var' => true,
    'show_in_rest' => true,
  );
  register_taxonomy('recipe_ingredients','recipes',$args);

}



//他にもっとシンプルな方法ないかな？
/* いらない説濃厚
add_filter( 'rewrite_rules_array', 'my_rewrite_rules_array' );
function my_rewrite_rules_array( $rules ) {
  $new_rules = array( 
    'recipes/([0-9]+)/?$' => 'index.php?post_type=recipes&p=$matches[1]',
  );
 
  return $new_rules + $rules;
}
*/




/////////// パーマリンク構造を変更 /////////// 

add_action('init', 'custom_posttype_rewrite');

function custom_posttype_rewrite() {

    global $wp_rewrite;

    $wp_rewrite->add_rewrite_tag('%recipes%', '(recipes)','post_type=');

    $wp_rewrite->add_permastruct('recipes', '/%recipes%/%post_id%/', false);

}

add_filter('post_type_link', 'custom_posttype_permalink', 1, 3);

function custom_posttype_permalink($post_link, $id = 0, $leavename) {

    global $wp_rewrite;

    $post = get_post($id); //&あったけど消した。大丈夫かな？

    if(is_wp_error( $post )){
        return $post;
    }

    if('recipes' === $post->post_type){
        $newlink = $wp_rewrite->get_extra_permastruct($post->post_type);

        $newlink = str_replace('%recipes%', $post->post_type, $newlink);

        $newlink = str_replace('%post_id%', $post->ID, $newlink);

        $newlink = home_url(user_trailingslashit($newlink));

        return $newlink;
    }

    return $post_link;
}


////////// レシピブロックを追加 //////////

function add_my_assets_to_block_editor() {
  wp_enqueue_style( 'block-style', get_stylesheet_directory_uri() . '/css/block_style.css' );
  wp_enqueue_script( 'block-custom', get_stylesheet_directory_uri() . '/js/block_custom.js',array(), "", true);
}
add_action( 'enqueue_block_editor_assets', 'add_my_assets_to_block_editor' );

////////// レシピ投稿タイプにレシピブロックをデフォルトで追加 //////////
function myplugin_register_template() {
  $post_type_object = get_post_type_object( 'recipes' );
  $post_type_object->template = [
    ['core/columns',[], [
      ['core/column',[], [
        ['core/image', [ 'className'=>"p-recipe-thumb"],'']
      ]],
      ['core/column',[], [
        ['core/paragraph',[
          'className'=>"p-recipe-desc",
          'placeholder'=> "キャッチコピーを入れてください"
        ]],
        //ここに再利用可能ブロックとしてnavをいれるあとでいいかあ
        ['core/heading',[ 'id'=>"ingredients",'className'=>"c-h2 m-icon m-ingredients m-font-black", 'content'=>"材料"]],
        ['core/table',[ 'className'=>"p-recipe-ingredients-table" ]],
      ]]
    ]],
    ['core/columns',[], [
      ['core/column',[], [
        ['core/heading',[ 'id'=>"directions",'className'=>"c-h2 m-icon m-directions m-font-black", 'content'=>"作り方"]],
        ['core/list',[ 'className'=>"p-recipe-directions-list" ]],
      ]],
      ['core/column',[], [
        ['core/group',['className'=>"p-recipe-points" ], [
          ['core/heading',[ 'id'=>"points",'className'=>"c-h2 m-icon m-points m-font-black", 'content'=>"ポイント"]],
          ['core/list',[ 'className'=>"p-recipe-points-list" ]],
        ]]
      ]]
    ]]
  ];

  //$post_type_object->template_lock = 'all'; 制限を入れない
}
add_action( 'init', 'myplugin_register_template' );


////////サムネイルの設定/////////
//アイキャッチ画像
add_theme_support( 'post-thumbnails', [ 'recipes' ] );


/* 基本サイズサイズを設定する場合 */
set_post_thumbnail_size( 260, 260, true );
 
/* 複数のサイズを設定する場合 */
add_image_size( 'big-thumbnail', 260, 260, true );




///////ページネーション////////

function pagination($pages = '', $range = 2){
  $showitems = ($range * 2)+1;
  global $paged;
  if(empty($paged)) $paged = 1;
  if($pages == '') {
      global $wp_query;
      $pages = $wp_query->max_num_pages;
      if(!$pages){
          $pages = 1;
      }
  }

  if(1 != $pages) {
    echo '<ul class="c-pagenation" role="menubar" aria-label="Pagination">';
    //echo '<li class="first"><a href="'.get_pagenum_link(1).'">＜＜</a></li>';
    //echo '<li class="prev"><a href="'.get_pagenum_link($paged - 1).'">＜</a></li>';
    for ($i=1; $i <= $pages; $i++) {
      if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )) {
        echo ($paged == $i)? 
          '<li class="e-item m-current"><a class="e-link">'.$i.'</a></li>':
          '<li class="e-item"><a class="e-link m-inactive" href="'.get_pagenum_link($i).'" >'.$i.'</a></li>';
      }
    }
    //echo '<li class="next"><a href="'.get_pagenum_link($paged + 1).'">＞</a></li>';
    //echo '<li class="last"><a href="'.get_pagenum_link($pages).'">＞＞</a></li>';
    echo '</ul>';
  }
}



///////////// レシピ特有の関数 //////////////
function get_recipe_terms($id){
  $genre_terms     = get_the_terms( $id , 'recipe_genre' ); 
  $season_terms    = get_the_terms( $id , 'recipe_season' ); 
  $condiment_terms = get_the_terms( $id , 'recipe_condiment' ); 
  $method_terms    = get_the_terms( $id , 'recipe_method' ); 
  $ingredients_terms= get_the_terms($id , 'recipe_ingredients' ); 

  $terms = array_merge(
    $genre_terms?:[],
    $season_terms?:[],
    $condiment_terms?:[],
    $method_terms?:[],
    $ingredients_terms?:[]
  );

  return $terms;

}

function get_queried_recipe_terms(){
  global $wp_query;
  $query_vars = $wp_query->query_vars;

  $taxonomies = ['recipe_genre','recipe_season','recipe_condiment','recipe_method','recipe_ingredients'];
  $queried_terms = [];
  $queried_term_slugs = [];

  foreach($taxonomies as $tax){
    if(isset($query_vars[$tax])){
      $term = get_term_by('slug', $query_vars[$tax], $tax, OBJECT);
      array_push($queried_terms, $term);
      array_push($queried_term_slugs, $term->slug);
    }
  }

  return [ $queried_terms, $queried_term_slugs];
}

function build_array_from_recipe_terms($recipe_terms){
  $array = [];
  foreach($recipe_terms as $term){
    $array[$term->taxonomy] = urldecode($term->slug);
  }
  return $array;
}

function build_query_from_recipe_terms($recipe_terms){
  $array = [];
  foreach($recipe_terms as $term){
    $array[$term->taxonomy] = urldecode($term->slug);
  }
  return http_build_query($array);
}

function build_json_from_recipe_terms($recipe_terms){
  $array = build_array_from_recipe_terms($recipe_terms);
  return json_encode($array, JSON_UNESCAPED_UNICODE);
}

function build_japanese_from_recipe_terms($recipe_terms){
  $str = "";

  $array = [
    'genre'=>null,
    'season'=>null,
    'condiment'=>null,
    'method'=>null,
    'ingredients'=>null
  ];

  foreach($recipe_terms as $term){
    // 変数名を短縮 recipe_genre => genre
    $array[explode('recipe_', $term->taxonomy)[1]] = $term->name;
  }

  extract($array);

  return
    ($ingredients ?: '') . (($condiment && $ingredients) ? 'と' : '' ) .  //食材
    ($condiment ? "「${condiment}」":'' ) . (($condiment || $ingredients) ? 'を使った' : '' ) .  //調味料
    ($season ? "${season}の":'').  //季節
    ($method ? "${method}":''). //方法
    ($genre  ?: '' ).  //ジャンル
    'レシピ' ; 
}


/////////トップページのメインクエリ書き換え ///////////

function twpp_change_sort_order( $query ) {
  if ( $query->is_main_query() && is_home()) {
    $query->set( 'posts_per_page', 30 );//全部表示
    $query->set( 'orderby', 'rand' );
    $query->set( 'post_type', 'recipes' );
  }
}

add_action( 'pre_get_posts', 'twpp_change_sort_order' );