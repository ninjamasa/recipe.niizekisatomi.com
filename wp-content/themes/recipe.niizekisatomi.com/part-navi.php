<?
  $is_inline = $args['is_inline'];
?>
<nav class="c-global-menu <?= $is_inline?'m-inline':''?>" id="<?= $is_inline?'':'global-menu'?>">
  <div class="e-shadow js-hide-menu"></div>
  <div class="e-content">
    <header class="e-header"> 
      <h2 class="e-title">かんたんレシピ検索</h2>
      <? if(!$is_inline): ?>
        <div class="e-close-button js-hide-menu"></div>
      <? endif;?>
    </header>
    <div class="e-group">
      <div class="c-search-tags">

        <h3 class="c-menu-title">レシピ名で検索</h3>
        <div class="e-content">
          <? get_search_form() ?>
        </div>

        <h3 class="c-menu-title">ジャンル</h3>
        <div class="e-content">
          <div class="grid-x grid-margin-x">
            <div class="cell small-6">
              <button class="c-button m-full" data-term="recipe_genre=tsukemono">漬物レシピ</button>
            </div>
            <div class="cell small-6">
              <button class="c-button m-full" data-term="recipe_genre=cuisine">料理レシピ</button>
            </div>
          </div>
        </div>
        <h3 class="c-menu-title">季節で選ぶ</h3>
        <div class="e-content">
          <div class="grid-x grid-margin-x">
            <div class="cell small-3">
              <button class="c-button m-full" data-term="recipe_season=spring">春</button>
            </div>
            <div class="cell small-3">
              <button class="c-button m-full" data-term="recipe_season=summer">夏</button>
            </div>
            <div class="cell small-3">
              <button class="c-button m-full" data-term="recipe_season=fall">秋</button>
            </div>
            <div class="cell small-3">
              <button class="c-button m-full" data-term="recipe_season=winter">冬</button>
            </div>
          </div>
        </div>
        <h3 class="c-menu-title">さとみの調味料で選ぶ</h3>
        <div class="e-content">
          <div class="grid-x grid-margin-x">
            <div class="cell small-4">
              <button class="c-button m-full" data-term="recipe_condiment=tare">魔法のたれ</button>
            </div>
            <div class="cell small-4">
              <button class="c-button m-full" data-term="recipe_condiment=shoyu">魔法の醤油</button>
            </div>
            <div class="cell small-4">
              <button class="c-button m-full" data-term="recipe_condiment=miso">18割味噌</button>
            </div>
          </div>
        </div>
        <h3 class="c-menu-title">つくり方で選ぶ</h3>
        <div class="e-content">
          <div class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-4">
              <button class="c-button m-full" data-term="recipe_method=sokuseki">かんたん</button>
            </div>
            <div class="cell small-4">
              <button class="c-button m-full" data-term="recipe_method=hitotema">ひと手間</button>
            </div>
            <div class="cell small-4">
              <button class="c-button m-full" data-term="recipe_method=hozon">つくりおき</button>
            </div>
            <div class="cell small-6">
              <button class="c-button m-full" data-term="recipe_method=dentou">伝統の味</button>
            </div>
            <div class="cell small-6">
              <button class="c-button m-full" data-term="recipe_method=arrange">新感覚</button>
            </div>
          </div>
        </div>
        <h3 class="c-menu-title">食材で選ぶ</h3>
        <div class="e-content">
          <? get_template_part("part-shokuzai") ?>
        </div>
      </div>
    </div>
    <div class="e-group">
      <h3 class="c-menu-title">姉妹サイト</h3>
      <div class="e-content">
        <div class="c-shimai grid-x grid-margin-x"><a class="e-col cell small-6" href="http://tsukemono.info" target="_blank">
            <h3 class="e-title">ショッピングサイト</h3>
            <p class="e-name">さとみの漬物講座・味噌講座</p><img class="e-banner" src="<?= get_stylesheet_directory_uri() ?>/img/banner_shopping.jpg"></a><a class="e-col cell small-6" href="http://new.niizekisatomi.com" target="_blank">
            <h3 class="e-title">プロフィールサイト</h3>
            <p class="e-name">新関さとみ.com</p><img class="e-banner" src="<?= get_stylesheet_directory_uri() ?>/img/banner_profile.jpg"></a></div>
      </div>
    </div>
  </div>
  <div class="e-cond-bar">
    <div class="c-cond-bar js-changeable">
      <div class="e-inner">
        <div class="e-conds">
          <!--
          .e-cond 冬
          .e-cond 野沢菜
          .e-cond 漬物
          
          -->
        </div>
        <form class="e-form" action="<?= home_url('/recipes') ?>" method="get">
          <div class="e-inputs"> 
            <!--ここにhiddenなinputをいれまくる-->
          </div>
          <button class="e-button" type="submit">レシピを<br>検索!</button>
        </form>
      </div>
    </div>
  </div>
</nav>