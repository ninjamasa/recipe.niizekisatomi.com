"use strict";

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

if (!window.RECIPE_INGREDIENTS_OPTIONS) {
  window.RECIPE_INGREDIENTS_OPTIONS = {
    ほうれんそう: {
      slug: 'ほうれんそう',
      name: 'ほうれんそう'
    },
    れんこん: {
      slug: 'れんこん',
      name: 'れんこん'
    },
    きゅうり: {
      slug: 'きゅうり',
      name: 'きゅうり'
    },
    なす: {
      slug: 'なす',
      name: 'なす'
    }
  };
}

$(function () {
  var RECIPE_STRUCT = {
    recipe_genre: {
      slug: 'recipe_genre',
      name: 'ジャンル',
      options: {
        cuisine: {
          slug: 'cuisine',
          name: '料理'
        },
        tsukemono: {
          slug: 'tsukemono',
          name: '漬物'
        }
      }
    },
    recipe_season: {
      slug: 'recipe_season',
      name: '季節',
      options: {
        spring: {
          slug: 'spring',
          name: '春'
        },
        summer: {
          slug: 'summer',
          name: '夏'
        },
        fall: {
          slug: 'fall',
          name: '秋'
        },
        winter: {
          slug: 'winter',
          name: '冬'
        }
      }
    },
    recipe_condiment: {
      slug: 'recipe_condiment',
      name: '調味料',
      options: {
        tare: {
          slug: 'tare',
          name: '魔法のたれ'
        },
        shoyu: {
          slug: 'shoyu',
          name: '魔法の醤油'
        },
        miso: {
          slug: 'miso',
          name: '18割こうじ味噌'
        }
      }
    },
    recipe_method: {
      slug: 'recipe_method',
      name: '漬け方 ',
      options: {
        sokuseki: {
          slug: 'sokuseki',
          name: 'かんたん'
        },
        hitotema: {
          slug: 'hitotema',
          name: 'ひと手間'
        },
        hozon: {
          slug: 'hozon',
          name: 'つくりおき'
        },
        dentou: {
          slug: 'dentou',
          name: '伝統の味'
        },
        arrange: {
          slug: 'arrange',
          name: '新感覚'
        }
      }
    },
    recipe_ingredients: {
      slug: 'recipe_ingredients',
      name: '食材',
      options: RECIPE_INGREDIENTS_OPTIONS
    }
  };
  console.log(RECIPE_STRUCT); //PHPから出力される

  window.RECIPE_QUERY = window.RECIPE_QUERY || {
    recipe_genre: null,
    //一個しか選べないことにする
    recipe_season: null,
    recipe_condiment: null,
    recipe_method: null,
    recipe_ingredients: null
  };

  function updateButtons(recipe_query) {
    Object.keys(recipe_query).forEach(function (tax) {
      var term = recipe_query[tax];
      $(".c-global-menu [data-term^=\"".concat(tax, "\"]")).removeClass("m-selected");

      if (term) {
        $(".c-global-menu [data-term=\"".concat(tax, "=").concat(term, "\"]")).addClass("m-selected");
      }
    });
  }

  updateButtons(RECIPE_QUERY);

  function updateCondBar(recipe_query) {
    var $conds = $(".c-cond-bar.js-changeable .e-conds");
    $conds.empty();
    Object.keys(recipe_query).forEach(function (tax) {
      var term = recipe_query[tax];
      var option = RECIPE_STRUCT[tax].options[term];

      if (!option) {
        return;
      }

      var termName = option.name;
      $conds.append("<div class='e-cond'>".concat(termName, "</div>"));
    });
    console.log($conds);
  }

  updateCondBar(RECIPE_QUERY);

  function updateCondBar(recipe_query) {
    //////検索条件の表示を変更////////
    var $conds = $(".c-cond-bar.js-changeable .e-conds");
    $conds.empty();
    var count = 0;
    Object.keys(recipe_query).forEach(function (tax) {
      var term = recipe_query[tax];
      var option = RECIPE_STRUCT[tax].options[term];

      if (!option) {
        return;
      }

      count++; //何個あるのか数える

      var termName = option.name;
      $conds.append("<div class='e-cond'>".concat(termName, "</div>"));
    }); //////formのinputsを変更////////

    var $inputs = $(".c-cond-bar.js-changeable .e-inputs");
    $inputs.empty();
    Object.keys(recipe_query).forEach(function (tax) {
      var term = recipe_query[tax];
      var option = RECIPE_STRUCT[tax].options[term];

      if (!option) {
        return;
      }

      $inputs.append("<input type=\"hidden\" name=\"".concat(tax, "\" value=\"").concat(option.slug, "\" />"));
    }); //////ボタンのテキストを変更////////

    var $button = $(".c-cond-bar.js-changeable .e-button");

    if (count == 0) {
      $button.html("全ての<br>レシピ");
    } else {
      $button.html("レシピを<br>検索!");
    }
  }

  updateCondBar(RECIPE_QUERY);
  $(".c-global-menu [data-term]").click(function () {
    //data-termの値からtaxonomyとtermを割り出して
    var _$$attr$split = $(this).attr("data-term").split("="),
        _$$attr$split2 = _slicedToArray(_$$attr$split, 2),
        taxonomy = _$$attr$split2[0],
        term = _$$attr$split2[1];

    if (term == RECIPE_QUERY[taxonomy]) {
      RECIPE_QUERY[taxonomy] = null;
    } else {
      RECIPE_QUERY[taxonomy] = term;
    }

    updateButtons(RECIPE_QUERY);
    updateCondBar(RECIPE_QUERY); //ボタンがぴかぴかアピール

    var $button = $(".c-cond-bar.js-changeable .e-button");
    $button.removeClass("m-attention");
    setTimeout(function () {
      $button.addClass("m-attention");
    }, 1);
  });
});