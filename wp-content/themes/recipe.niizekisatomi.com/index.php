<?
  get_header();
?>
    <ul class="p-top-recipe-grid">
      <?
        while(have_posts()):
          the_post();
      ?>
      
      <li class="e-item"><a class="e-link" href="<?the_permalink()?>">
          <? if(has_post_thumbnail()): 
           the_post_thumbnail("big-thumbnail", ['class'=>'e-image']);
          else: ?><img class="e-image" src="https://placehold.jp/200x200.png" alt="" width="200" height="200"><? endif; ?>
          <h3 class="e-title"><? the_title() ?></h3></a></li><? endwhile; ?>
    </ul>
    <div class="p-top-recipe-type">
      <div class="e-inner">
        <div class="e-col"><a class="c-button m-border m-full" href="<?=home_url('/recipes?recipe_genre=tsukemono')?>"> <img class="e-icon" src="<?= get_stylesheet_directory_uri() ?>/img/icon_kabu.svg">漬物レシピ</a></div>
        <div class="e-col"><a class="c-button m-border m-full" href="<?=home_url('/recipes?recipe_genre=cuisine')?>"> <img class="e-icon" src="<?= get_stylesheet_directory_uri() ?>/img/icon_owan.svg">料理レシピ</a></div>
      </div>
    </div>
    <div class="grid-container">
      <section class="c-section">
        <h2 class="c-h2">季節で選ぶ</h2>
        <nav class="p-top-kisetsu u-text-align-center">
          <div class="e-shun">
            <div class="e-title">今が旬！</div>
            <div class="e-banner">
              <? $month = intval(date('m')); ?>
              <? if(3 <= $month  && $month <= 5): ?>
                <a href="<?=home_url('/recipes/?recipe_season=spring')?>"><img src="<?= get_stylesheet_directory_uri() ?>/img/season_spring.jpg"></a>
              <? elseif(6 <= $month  && $month <= 8): ?>
                <a href="<?=home_url('/recipes/?recipe_season=summer')?>"><img src="<?= get_stylesheet_directory_uri() ?>/img/season_summer.jpg"></a>
              <? elseif(9 <= $month  && $month <= 11): ?>
                <a href="<?=home_url('/recipes/?recipe_season=fall')?>">  <img src="<?= get_stylesheet_directory_uri() ?>/img/season_fall.jpg">  </a>
              <? else: ?>
                <a href="<?=home_url('/recipes/?recipe_season=winter')?>"><img src="<?= get_stylesheet_directory_uri() ?>/img/season_winter.jpg"></a>
              <? endif?>
            </div>
            <div class="e-balance"> </div>
          </div>
          <div class="e-banners grid-x grid-margin-x grid-margin-y">
            <div class="e-cell cell small-6 medium-5 medium-offset-1"><a class="e-link" href="<?=home_url('/recipes/?recipe_season=spring')?>"><img class="e-image" src="<?= get_stylesheet_directory_uri() ?>/img/season_spring.jpg"></a></div>
            <div class="e-cell cell small-6 medium-5"><a class="e-link" href="<?=home_url('/recipes/?recipe_season=summer')?>"><img class="e-image" src="<?= get_stylesheet_directory_uri() ?>/img/season_summer.jpg"></a></div>
            <div class="e-cell cell small-6 medium-5 medium-offset-1"><a class="e-link" href="<?=home_url('/recipes/?recipe_season=fall')?>"><img class="e-image" src="<?= get_stylesheet_directory_uri() ?>/img/season_fall.jpg"></a></div>
            <div class="e-cell cell small-6 medium-5"><a class="e-link" href="<?=home_url('/recipes/?recipe_season=winter')?>"><img class="e-image" src="<?= get_stylesheet_directory_uri() ?>/img/season_winter.jpg"></a></div>
          </div>
        </nav>
      </section>
      <section class="c-section">
        <h2 class="c-h2">さとみの調味料で選ぶ</h2>
        <div class="grid-x grid-margin-x u-mb-3">
          <div class="e-cell cell small-4"><a class="c-button m-border m-full m-high" href="<?=home_url('/recipes?recipe_condiment=tare')?>">魔法のたれ<br>で作る</a></div>
          <div class="e-cell cell small-4"><a class="c-button m-border m-full m-high" href="<?=home_url('/recipes?recipe_condiment=shoyu')?>">魔法の醤油<br>で作る</a></div>
          <div class="e-cell cell small-4"><a class="c-button m-border m-full m-high" href="<?=home_url('/recipes?recipe_condiment=miso')?>">18割こうじ味噌<br>で作る</a></div>
        </div>
        <div class="p-top-osusume u-full-width">
          <div class="e-title">
            <div class="e-icon"><img class="e-image" src="<?= get_stylesheet_directory_uri() ?>/img/logo_satomi.svg"></div>
            <p class="e-osusume">さとみおすすめ！</p>
            <p class="e-kuwashiku">詳しくはこちら</p>
          </div>
          <div class="e-banners grid-x grid-margin-x">
            <div class="e-cell cell small-4"><a class="e-link c-button m-border m-full m-high" href="https://new.niizekisatomi.com/product/mahou-tare/" target="_blank">魔法のたれ</a></div>
            <div class="e-cell cell small-4"><a class="e-link c-button m-border m-full m-high" href="https://new.niizekisatomi.com/product/mahou-shoyu/" target="_blank">魔法の醤油</a></div>
            <div class="e-cell cell small-4"><a class="e-link c-button m-border m-full m-high" href="https://new.niizekisatomi.com/product/miso/" target="_blank">18割こうじ<br>味噌</a></div>
          </div>
        </div>
      </section>
      <section class="c-section">
        <h2 class="c-h2">漬物を漬け方で選ぶ</h2>
        <div class="grid-x grid-margin-x grid-margin-y u-text-align-center">
          <div class="cell small-6 medium-5 medium-offset-1"><a class="e-link" href="<?=home_url('/recipes?recipe_method=sokuseki')?>"><img src="<?= get_stylesheet_directory_uri() ?>/img/method_sokuseki.jpg" alt="即席漬"></a></div>
          <div class="cell small-6 medium-5"><a class="e-link" href="<?=home_url('/recipes?recipe_method=hozon')?>"><img src="<?= get_stylesheet_directory_uri() ?>/img/method_hozon.jpg" alt="保存漬"></a></div>
          <div class="cell small-6 medium-5 medium-offset-1"><a class="e-link" href="<?=home_url('/recipes?recipe_method=dentou')?>"><img src="<?= get_stylesheet_directory_uri() ?>/img/method_dentou.jpg" alt="伝統の味"></a></div>
          <div class="cell small-6 medium-5"><a class="e-link" href="<?=home_url('/recipes?recipe_method=arrange')?>"><img src="<?= get_stylesheet_directory_uri() ?>/img/method_arrange.jpg" alt="アレンジ"></a></div>
        </div>
      </section>
      <section class="c-section">
        <h2 class="c-h2">食材で選ぶ</h2>
        <? get_template_part("part-shokuzai", null, ["is_index"=>true]) ?>
      </section>
      <section class="c-section">
        <h2 class="c-h2">姉妹サイト</h2>
        <div class="c-shimai grid-x grid-margin-x"><a class="e-col cell small-7 medium-6" href="http://tsukemono.info" target="_blank">
            <h3 class="e-title">ショッピングサイト</h3>
            <p class="e-name">さとみの漬物講座・味噌講座</p><img class="e-banner" src="<?= get_stylesheet_directory_uri() ?>/img/banner_shopping.jpg"></a><a class="e-col cell small-5 medium-6" href="http://new.niizekisatomi.com" target="_blank">
            <h3 class="e-title">プロフィールサイト</h3>
            <p class="e-name">新関さとみ.com</p><img class="e-banner" src="<?= get_stylesheet_directory_uri() ?>/img/banner_profile.jpg"></a></div>
      </section>
      <section class="p-top-osagashi c-section"> 
        <header class="e-header">
          <h2 class="e-heading">何をお探しですか？</h2>
          <p class="e-desc">タップするとご案内が開きます。</p>
        </header>
        <dl class="e-def-list">
          <dt class="e-title">新関さとみとは？どんな会社？</dt>
          <dd class="e-content">
            <div class="e-inner">
              <p>さとみの漬物講座企業組合　理事長。横浜市生まれ。父のＵターンと共に山形県天童市で育ち、大学・ＯＬ時代は東京へ。２０代後半で自身もＵターンし、山二醤油醸造三代目に嫁ぐ。義母から漬物作りを学び、起業。	</p>
              <div class="e-button-wrapper"><a class="e-button" href="http://new.niizekisatomi.com/profile/" target="_blank">新関さとみのプロフィールへ	</a></div>
            </div>
          </dd>
          <dt class="e-title">さとみを取り巻く歴史	</dt>
          <dd class="e-content">
            <div class="e-inner">
              <p>醤油と味噌の醸造元「山二醤油醸造㈱」は、山形市西部地区大曽根で昭和２年に創業。創業者「徳次郎」の意思を継ぎながら、時代の流れと共に現社長三代目「徳次郎」と共に頑張っています。	</p>
              <div class="e-button-wrapper"><a class="e-button" href="https://tsukemono.info/SHOP/994345/994355/list.html" target="_blank">山二醤油醸造の歴史	</a></div>
            </div>
          </dd>
          <dt class="e-title">商品を買いたい！	</dt>
          <dd class="e-content">
            <div class="e-inner">
              <p>山形の懐かしい味わいを継承しつつ、現代人の好みに合う「ほんのり甘くて、塩気のやさしい」醤油・味噌・漬物などの商品作りを目指しています。漬物が簡単に美味しく出来る「魔法のたれ」、これ一本で料理の味わいがぐっとよくなる減塩あまくちの「魔法のしょうゆ」、山形県産大豆・米で作るこうじたっぷり甘くて減塩の「18割こうじ贅沢みそ」。その他、漬物・料理のレシピ本などをお求めになれます。お支払い方法は、クレジット、コンビニ払い、代引き、郵便振替からお選びいただけます。ご注文から3営業日以内に発送致します。	</p>
              <div class="e-button-wrapper"><a class="e-button" href="https://tsukemono.info/index.html" target="_blank">さとみの漬物講座・味噌講座ショッピングサイト	</a></div>
            </div>
          </dd>
          <dt class="e-title">味噌作り講座について知りたい！	</dt>
          <dd class="e-content">
            <div class="e-inner">
              <p>山形県産大豆・米を使った天然醸造の味噌作り。こうじたっぷりの甘くて減塩の味噌を作ります。親子学習会、公民館事業、職場の福利厚生など山形県内、宮城県・福島県などに材料一式を持ってお伺いします。食育価格もございますので、気軽にお問い合わせください。	</p>
              <div class="e-button-wrapper"><a class="e-button" href="http://new.niizekisatomi.com/kouza/miso/" target="_blank">さとみの味噌作り講座とは</a></div>
            </div>
          </dd>
          <dt class="e-title">漬物講座について知りたい！	</dt>
          <dd class="e-content">
            <div class="e-inner">
              <p>旬の野菜を使った、家庭で漬ける漬物の作り方をお伝えします。懐かしい我が家の味わいを再現できる技、コツを知るとあなたも漬物名人に！講座で作る漬物はお持ち帰り出来、最後に旬のお漬物の試食会もございます。	</p>
              <div class="e-button-wrapper"><a class="e-button" href="https://new.niizekisatomi.com/kouza/tsukemono/" target="_blank">さとみのかんたん漬物講座とは	</a></div>
            </div>
          </dd>
          <dt class="e-title">「大曽根餅つき保存会」餅つきについて知りたい！	</dt>
          <dd class="e-content">
            <div class="e-inner">
              <p>夫が会長を務める「大曽根餅つき保存会」。人が杵と臼で搗く本物の餅を味わってもらいたいと平成15年に発足しました。「ＴＶチャンピオンの全国餅つき王選手権」に２年連続出場。杵臼、幻のもち米「晩白玉」、納豆、納豆用の醤油「もちだまり」、きな粉をご用意して伺いますので、すぐに餅つきを楽しめます。	</p>
              <div class="e-button-wrapper"><a class="e-button" href="https://niizekisatomi.com/mochitaro/" target="_blank">大曽根餅つき保存会へ	</a></div>
            </div>
          </dd>
          <dt class="e-title">新関さとみの著書について知りたい！	</dt>
          <dd class="e-content">
            <div class="e-inner">
              <p>「ビニール袋で漬ける！」の元祖のレシピ本です。全国出版のレシピ本は３万部突破でベストセラーと言われる中、この本はなんと19刷の4万部突破（令和2年1月現在）。手軽に簡単に漬けられる量とコツが満載の、漬物好きのバイブルともいえる本です。女性カメラマンの写真も綺麗で見ているだけで癒されます。</p>
              <div class="e-button-wrapper"><a class="e-button" href="https://tsukemono.info/SHOP/994349/994364/list.html" target="_blank">「ビニール袋で手早くできる野菜のうまみが活きる漬物」レシピ本とは	</a></div>
            </div>
          </dd>
        </dl>
      </section>
    </div>

<?
  get_footer();
?>