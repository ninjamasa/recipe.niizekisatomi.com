<? if($wp_query->found_posts == 0){ header('HTTP/1.0 404 Not Found'); } ?>
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="format-detection" content="telephone=no">
    <!--title= PAGE_TITLE //WPが出してくれるので使わない-->
    <!--link(rel="canonical", href=PAGE_URL) //各ページで設定--><?
      $SITE_NAME   = get_bloginfo('name');
      $PAGE_TITLE  = get_bloginfo('name'); //要変更
    
      $DESCRIPTION = get_bloginfo('description');

      if(is_single()){
        $desc = get_post_meta($post->ID, "description", true);

        if($desc){
          $DESCRIPTION = $desc;
        }

      }
      $PAGE_URL    = home_url('/'); //要変更
      $OGP_IMAGE   = get_stylesheet_directory_uri() . "/img/icons/ogp.jpg";
    ?>
    
    <meta name="description" content="<?= $DESCRIPTION ?>">
    <meta property="og:site_name" content="<?= $SITE_NAME ?>">
    <meta property="og:title" content="<?= $PAGE_TITLE ?>">
    <meta property="og:description" content="<?= $DESCRIPTION ?>">
    <meta property="og:type" content="article">
    <meta property="og:url" content="<?= $PAGE_URL ?>">
    <meta property="og:image" content="<?= $OGP_IMAGE ?>">
    <meta property="og:image:secure_url" content="<?= $OGP_IMAGE ?>">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="<?= $PAGE_TITLE ?>">
    <meta name="twitter:description" content="<?= $DESCRIPTION ?>">
    <meta name="twitter:image" content="<?= $OGP_IMAGE ?>">
    <link rel="stylesheet" href="<?= get_stylesheet_directory_uri() ?>/css/style.css">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= get_stylesheet_directory_uri() ?>/icons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= get_stylesheet_directory_uri() ?>/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= get_stylesheet_directory_uri() ?>/icons/favicon-16x16.png">
    <link rel="manifest" href="<?= get_stylesheet_directory_uri() ?>/icons/site.webmanifest">
    <link rel="mask-icon" href="<?= get_stylesheet_directory_uri() ?>/icons/safari-pinned-tab.svg" color="#da532c">
    <link rel="shortcut icon" href="<?= get_stylesheet_directory_uri() ?>/icons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="<?= get_stylesheet_directory_uri() ?>/icons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-156965150-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-156965150-1');
     
    </script>
    <?
      if(is_home()):
    ?>
      <title><?= bloginfo('name')?></title>
      <link rel="canonical" href="<?= home_url('/')?>">
      <meta name="apple-mobile-web-app-title" content="さとみのレシピ">
    <?
      elseif(is_single() || is_archive()):
        [$queried_terms, $queried_term_slugs] = get_queried_recipe_terms();

        //wpで使いやすいように
        $term_array = build_array_from_recipe_terms($queried_terms);
        //リンクにつけるために、逆に、url queryを生成 recipe_genre=cuisine&recipe_season=spring みたいな
        $term_query = build_query_from_recipe_terms($queried_terms);
        //jsonを生成
        $query_json = build_json_from_recipe_terms($queried_terms);
        //自然な日本語を生成
        $query_japanese = build_japanese_from_recipe_terms($queried_terms);
    ?>
      <!--JSに渡す-->
      <script>
        window.RECIPE_QUERY = <?= $query_json?>;
        console.log({RECIPE_QUERY:window.RECIPE_QUERY})
        //このあと、global_menu.jsで、RECIPE_QUERYを使って メニューが色付けされる
      </script>

      <?
        if(is_single()):
      ?>
        <title><? the_title() ?> | <?= $query_japanese ?> | <?= bloginfo('name')?></title>
        <!--meta(name="description" content!="<?= bloginfo('description')?>") ここに、キャッチコピーの内容を持ってきたいけど、できるのかしら？-->
        <link rel="canonical" href="<?= the_permalink() ?>">
      <?
        elseif(is_archive()):
      ?>

        <title><?= $query_japanese ?> | <?= bloginfo('name')?></title>
        <link rel="canonical" href="<?= home_url('/recipes/?'.$term_query)?>">

      <?
        endif
      ?>

    <?
      elseif(is_search()):
    ?>
      <title>"<?= $wp_query->query['s'] ?>"の検索結果 | <?= bloginfo('name')?></title>
    <?
      else:
    ?>
      <title><?= "不明なページ |  ". bloginfo('name')?></title>
    <?
      endif;
    ?>

    <?php wp_head(); ?>
  </head>
  <body>
    <!-- Google Tag Manager (noscript)-->
    <noscript>
      <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9P8W47" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript)-->
    <header class="c-top-bar <?= is_user_logged_in()?'m-admin-bar':'' ?>">
      <div class="e-inner"><a class="e-top-link" href="<?=home_url('/')?>">
          <div class="e-icon"><img src="<?=get_stylesheet_directory_uri() ?>/img/logo_satomi.svg" alt="さとみロゴ"></div>
          <h1 class="e-titles">
            <div class="e-satomino">新関さとみの</div>
            <div class="e-recipe">田舎ごはんレシピ</div>
            <div class="e-tsukemono-ryori">漬物・料理</div>
          </h1></a>
        <nav class="e-buttons"><a class="e-button m-tel" href="tel:023-643-2513">
            <div class="e-icon-title">
              <div class="e-icon"><img src="<?= get_stylesheet_directory_uri() ?>/img/icon_tel.svg" alt="お電話アイコン"></div>
              <div class="e-title">お電話</div>
            </div>
            <div class="e-content m-tel">023-643-2513</div></a>
          <div class="e-button m-search">
            <button class="e-icon-title js-show-menu">
              <div class="e-icon"><img src="<?= get_stylesheet_directory_uri() ?>/img/icon_search.svg" alt="レシピ検索アイコン"></div>
              <div class="e-title">レシピ検索</div>
            </button>
            <div class="e-content m-search">
              <div class="c-cond-bar m-top-bar js-changeable">
                <div class="e-inner">
                  <div class="e-conds js-show-menu">
                    <!-- jsで入れてもらえます
                    .e-cond 冬
                    .e-cond 野沢菜
                    .e-cond 漬物
                    
                    -->
                  </div>
                  <form class="e-form" action="<?= home_url('/recipes') ?>" method="get">
                    <div class="e-inputs"> 
                      <!--ここにhiddenなinputをいれまくる-->
                    </div>
                    <button class="e-button" type="submit">レシピを<br>検索!</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </nav>
      </div>
    </header>
  <hr class="c-top-spacer">
  <?
  get_template_part("part-navi", null, ["is_inline"=>false]);
  ?>