# 更新のしかた
## pug 

pugは重複なくいろいろ書けるため便利なので使います。

## pug/scssコンパイルの仕方
あらかじめ gulpfile.js にいろいろ書いているので、このディレクトリで下記のコマンドを実行すればいろいろ動き出します。

```
$ yarn          #npm package をインストール。 node_modules ができる。
$ yarn gulp     #local gulpによりpugとscssをコンパイル。 watch や browser sync も起動します。
```

## Foundation について
FoundationはSassのフレームワークです。あらかじめ便利なクラスが定義されているので使える時は使えます。 
https://get.foundation/sites/docs/ 

特に、下記のgridが便利なはず 
https://get.foundation/sites/docs/xy-grid.html 

## pugのファイル構成

```
src
  pug
    inc
      footer.pug  # フッター共通部分をやってます。
    layout
      struct.pug  # <head>の中身などの構造が書いてあります
    input.pug     # input.html に変換されます。 LP のコンテンツとフォーム入力部分があります。
    confirm.pug   # confirm.html に変換されます。 フォーム内容確認ページです。
    finish.pug    # finish.html に変換されます。 フォーム送信完了ページです。
```

## scssのファイル構成

```
src
  scss
    project
      _project.scss    # p-ではじまる、使い回ししなさそうなパーツです。
      _xxx.scss        # p-ではじまる、各ページでしか使わないパーツです。

    component
      _component.scss  # c-ではじまる使い回ししそうなパーツです。

    utility
      _xxxx.scss       # xxxxというCSSプロパティに関する utility クラスたちです。この子たちは例外的に!important をつけます。

    style.scss         # 上記のファイルをここでimport してまとめています。  /css/style.css に変換されます。

```