<?
  get_header();

  //header と処理が重複しているが、globalよりマシと判断
  [$queried_terms, $queried_term_slugs] = get_queried_recipe_terms();

  //wpで使いやすいように
  $term_array = build_array_from_recipe_terms($queried_terms);
  //リンクにつけるために、逆に、url queryを生成 recipe_genre=cuisine&recipe_season=spring みたいな
  $term_query = build_query_from_recipe_terms($queried_terms);
  //jsonを生成
  $query_json = build_json_from_recipe_terms($queried_terms);
  //自然な日本語を生成
  $query_japanese = build_japanese_from_recipe_terms($queried_terms);

?>
    <div class="l-main-sub">
      <div class="e-main">
        <div class="grid-container">

          <?
            $s = isset( $wp_query->query['s'] ) ?  $wp_query->query['s'] : null; // 検索にも対応
            
          ?>


          <ol class="p-recipe-bc">
            <li class="e-item"><a class="e-link" href="/">トップ</a></li>
            <li class="e-item"><a class="e-link m-now" href="<?= home_url('/recipes/?'.$term_query)?>">
                 <?
                  if(count($queried_terms) != 0){
                    echo $query_japanese;
                    if($wp_query->found_posts == 0){
                      echo "は見つかりませんでした💦";
                    }
                  }
                  else if($s){
                    echo "\"$s\"の検索結果";
                  }
                  else{
                    echo '全てのレシピ';
                  }
                ?>
                </a></li>
          </ol>
        </div>

        <h2 class="c-cond-bar m-heading js-show-menu u-mb-2 u-mt-0">
          <div class="e-inner">
            <div class="e-conds">

              <? if ($s): ?>
                <div class="e-cond"><?= '"'. $_GET['s'] .'"' ?></div>
              <? endif; ?>

              <? foreach($queried_terms as $term): ?>
                <div class="e-cond"><?= $term->name ?></div>
              <? endforeach; ?>
              <div class="e-norecipe"><?= (count($queried_terms) != 0 || $s) ? "の":"全ての"?>レシピ一覧(<?=$wp_query->found_posts?>件)</div>
            </div>
          </div>
        </h2>
        <div class="grid-container">
          <?php
            if(have_posts()):
          ?>
          <ul class="p-archive-recipes">
            <?php
              while(have_posts()):
                the_post();
            ?>
            <li class="e-item"><a class="e-link" href="<?= the_permalink() . '?' . $term_query?>">
                <? if(has_post_thumbnail()): 
                 the_post_thumbnail("big-thumbnail", ['class'=>'e-thumb']);
                else: ?><img class="e-thumb" src="https://placehold.jp/200x200.png" alt="" width="200" height="200"><? endif; ?>
                <h3 class="e-title"><? the_title() ?></h3><?php
                  $terms = get_recipe_terms(0);
                ?>
                
                <ul class="e-tags"><? foreach($terms as $term ):  ?>
                  <li class="e-tag <?= in_array($term->slug, $queried_term_slugs)?'m-current':'' ?>"><?= $term->name ?></li><? endforeach; ?>
                </ul></a></li><? endwhile; ?>
          </ul><? else: ?>
          <p class="u-text-align-center u-mt-10 u-mb-10"><?= build_japanese_from_recipe_terms($queried_terms);?> <br class="show-for-small hide-for-large">は見つかりませんでした💦<br>検索条件を減らしてみてください🙇‍♀️</p><? endif; ?>
        </div><?
        
          if (function_exists('pagination')) { // functions.php に定義
            pagination($wp_query->max_num_pages);
          }
          else{
            echo "pagenation not found";
          }
        
          wp_reset_query();
        ?>
      </div>
      <div class="e-sub">
        <? get_template_part("part-navi", null, ["is_inline"=>true]) ?>
      </div>
      <div class="e-bottom">
        <div class="grid-container fluid" id="banners"> 
          <h2 class="c-h2 m-font-black">さとみの調味料のご購入はこちら <br class="show-for-small-only"><small>(ショッピングサイトを別タブで開きます)</small></h2><?
          
            $terms_array = build_array_from_recipe_terms($queried_terms);
            $condiment_term_slug = isset($terms_array['recipe_condiment'])? $terms_array['recipe_condiment']: '';
            $using_shoyu = false;
            $using_tare= false;
            $using_miso= false;
          
            if($condiment_term_slug == 'shoyu'){
              $using_shoyu = true;
            }
            if($condiment_term_slug == 'tare'){
              $using_tare = true;
            }
            if($condiment_term_slug == 'miso'){
              $using_miso = true;
            }
          ?>
          
          <nav class="c-osusume-banners"><a class="<?= $using_shoyu ? 'm-using':'' ?> e-item" href="https://new.niizekisatomi.com/product/mahou-shoyu/" target="_blank">
              <h3 class="c-h3">魔法の醤油</h3>
              <p><img src="<?=get_stylesheet_directory_uri()?>/img/banner_shoyu.jpg"></p></a><a class="<?= $using_miso ? 'm-using':'' ?> e-item" href="https://new.niizekisatomi.com/product/miso/" target="_blank">
              <h3 class="c-h3">18割こうじ味噌</h3>
              <p><img src="<?=get_stylesheet_directory_uri()?>/img/banner_miso.jpg"></p></a><a class="<?= $using_tare ? 'm-using':'' ?> e-item" href="https://new.niizekisatomi.com/product/mahou-tare/" target="_blank">
              <h3 class="c-h3">魔法のたれ</h3>
              <p><img src="<?=get_stylesheet_directory_uri()?>/img/banner_tare.jpg"></p></a><a class="e-item" href="https://tsukemono.info/SHOP/994349/994364/list.html" target="_blank">
              <h3 class="c-h3">レシピ本</h3>
              <p><img src="<?=get_stylesheet_directory_uri()?>/img/banner_recipe_book_2.jpg"></p></a></nav>
        </div>
      </div>
    </div>
<?
  get_footer();
?>