    <div class="c-footer">
      <div class="e-inner grid-container">
        ※当サイトのレシピ写真は「ビニール袋で手早くできる！野菜のうまみの活きる漬け物」（カメラマン：牧野純子）と
        「おかずに美味しい田舎ごはん」（カメラマン：菊地和男）より。
        <hr class="c-hr u-mt-1 u-mb-1">Copyright (C) Satomi no Tsukemono Koza. All rights reserved.　<br>当サイトの内容・構成・デザイン・画像の無断転載、複製（コピー)は一切禁止しています。
      </div>
    </div><? wp_footer()?>
    <script src="<?= get_stylesheet_directory_uri() ?>/js/jquery.min.js"></script><?
      $ingredients_terms = get_terms('recipe_ingredients', [
        'orderby' => 'count',
        'order' => 'desc',
      ]);
    
    
      $terms_object = [];
    
      foreach( $ingredients_terms as $term ){
        $slug = urldecode($term->slug);
        $terms_object[$slug] = [
          'slug'=>$slug,
          'name'=>$term->name,
        ];
      }
    
      $ingredients_terms_json = json_encode($terms_object, JSON_UNESCAPED_UNICODE);
      
    ?>
    <script>
      window.RECIPE_INGREDIENTS_OPTIONS = <?= $ingredients_terms_json ?>;
      
    </script>
    <script src="<?= get_stylesheet_directory_uri() ?>/js/global_menu.js"></script>
    <script src="<?= get_stylesheet_directory_uri() ?>/js/anchor_scroll.js"></script>
    <script>
      $(function($){
        $(".c-shokuzai-tags>.e-open-button").click(function(e){
          $(this).parent().toggleClass("m-opened")
        });
      
        $(".js-show-menu").click(function(e){
          $("#global-menu").addClass("m-shown");
        })
      
        $(".js-hide-menu").click(function(e){
          $("#global-menu").removeClass("m-shown");
        })
      
      });
      
      
    </script>

    <?
      if(is_home()):
    ?>
      <script>
        $(function($){
        
          $(".p-top-osagashi>.e-def-list>.e-title").click(function(e){
            $(this).toggleClass("m-opened")
          });
        
        
          (function(){
            var $recipeGrid = $(".p-top-recipe-grid");
        
            function cancelScroll(e){
              if(Math.abs(e.originalEvent.deltaX) <= 1){
                return;
              }
              clearInterval(scroller);
            }
        
            $recipeGrid.on('touchstart wheel mousewheel DOMMouseScroll', cancelScroll);
        
            var i = 0;
            var vel = 1;
            function scroll(){
              i+=vel;
              $recipeGrid.scrollLeft(i);
        
              if($recipeGrid.scrollLeft() > 1000 || $recipeGrid.scrollLeft() < 0){
                vel *= -1;
                i += vel;
              }
            }
            var scroller = setInterval(scroll, 1000/30)
          })();
        });
      </script>
    <?
      endif;
    ?>

  </body>
</html>