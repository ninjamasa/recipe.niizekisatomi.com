
if(!window.RECIPE_INGREDIENTS_OPTIONS ){

  window.RECIPE_INGREDIENTS_OPTIONS = {
    ほうれんそう:{
      slug: 'ほうれんそう',
      name: 'ほうれんそう'
    },
    れんこん:{
      slug: 'れんこん',
      name: 'れんこん'
    },
    きゅうり:{
      slug: 'きゅうり',
      name: 'きゅうり'
    },
    なす:{
      slug: 'なす',
      name: 'なす'
    },
  }
}


$(function(){
  const RECIPE_STRUCT = {
    recipe_genre: {
      slug: 'recipe_genre',
      name: 'ジャンル',
      options:{
        cuisine:{
          slug: 'cuisine',
          name: '料理'
        },
        tsukemono:{
          slug: 'tsukemono',
          name: '漬物'
        }
      }
    },
    recipe_season : {
      slug: 'recipe_season',
      name: '季節',
      options:{
        spring:{
          slug: 'spring',
          name: '春'
        },
        summer:{
          slug: 'summer',
          name: '夏'
        },
        fall:{
          slug: 'fall',
          name: '秋'
        },
        winter:{
          slug: 'winter',
          name: '冬'
        },
      }
    },

    recipe_condiment: {
      slug: 'recipe_condiment',
      name: '調味料',
      options:{
        tare :{
          slug: 'tare',
          name: '魔法のたれ'
        },
        shoyu:{
          slug: 'shoyu',
          name: '魔法の醤油'
        },
        miso:{
          slug: 'miso',
          name: '18割こうじ味噌'
        }
      }
    },
    recipe_method: {
      slug: 'recipe_method',
      name: '漬け方 ',
      options:{
        sokuseki:{
          slug: 'sokuseki',
          name: 'かんたん'
        },
        hitotema:{
          slug: 'hitotema',
          name: 'ひと手間'
        },
        hozon:{
          slug: 'hozon',
          name: 'つくりおき'
        },
        dentou:{
          slug: 'dentou',
          name: '伝統の味'
        },
        arrange:{
          slug: 'arrange',
          name: '新感覚'
        },
      }
    },
    recipe_ingredients: {
      slug: 'recipe_ingredients',
      name: '食材',
      options: RECIPE_INGREDIENTS_OPTIONS
    }

  };

  console.log(RECIPE_STRUCT);

  //PHPから出力される
  window.RECIPE_QUERY = window.RECIPE_QUERY || {
    recipe_genre: null, //一個しか選べないことにする
    recipe_season: null,
    recipe_condiment: null,
    recipe_method: null,
    recipe_ingredients: null,
  };

  function updateButtons(recipe_query){
    Object.keys(recipe_query).forEach((tax)=>{
      const term = recipe_query[tax];

      $(`.c-global-menu [data-term^="${tax}"]`).removeClass("m-selected");

      if(term){
        $(`.c-global-menu [data-term="${tax}=${term}"]`).addClass("m-selected");
      }
    })
  }

  updateButtons(RECIPE_QUERY);

  function updateCondBar(recipe_query){
    const $conds = $(".c-cond-bar.js-changeable .e-conds");

    $conds.empty();

    Object.keys(recipe_query).forEach((tax)=>{
      const term = recipe_query[tax];

      const option = RECIPE_STRUCT[tax].options[term];
      if(!option){
        return;
      }

      const termName = option.name;
      $conds.append(`<div class='e-cond'>${termName}</div>`);
    });


    console.log($conds);
  }

  updateCondBar(RECIPE_QUERY);

  function updateCondBar(recipe_query){
    //////検索条件の表示を変更////////
    const $conds = $(".c-cond-bar.js-changeable .e-conds");

    $conds.empty();

    let count = 0;
    Object.keys(recipe_query).forEach((tax)=>{
      const term = recipe_query[tax];

      const option = RECIPE_STRUCT[tax].options[term];
      if(!option){
        return;
      }

      count ++; //何個あるのか数える

      const termName = option.name;
      $conds.append(`<div class='e-cond'>${termName}</div>`);
    });

    //////formのinputsを変更////////
    const $inputs = $(".c-cond-bar.js-changeable .e-inputs");
    $inputs.empty();
    Object.keys(recipe_query).forEach((tax)=>{
      const term = recipe_query[tax];

      const option = RECIPE_STRUCT[tax].options[term];
      if(!option){
        return;
      }

      $inputs.append(`<input type="hidden" name="${tax}" value="${option.slug}" />`);
    });

    //////ボタンのテキストを変更////////
    const $button = $(".c-cond-bar.js-changeable .e-button");

    if(count == 0){
      $button.html("全ての<br>レシピ")
    }
    else{
      $button.html("レシピを<br>検索!")
    }
  }

  updateCondBar(RECIPE_QUERY);


  $(".c-global-menu [data-term]").click(function(){
    //data-termの値からtaxonomyとtermを割り出して
    const [taxonomy, term] = $(this).attr("data-term").split("=");

    if(term == RECIPE_QUERY[taxonomy]){
      RECIPE_QUERY[taxonomy] = null;
    }
    else{
      RECIPE_QUERY[taxonomy] = term;
    }


    updateButtons(RECIPE_QUERY);
    updateCondBar(RECIPE_QUERY);

    //ボタンがぴかぴかアピール
    const $button = $(".c-cond-bar.js-changeable .e-button");
    $button.removeClass("m-attention");
    setTimeout(()=>{
      $button.addClass("m-attention");
    },1);
  })


})