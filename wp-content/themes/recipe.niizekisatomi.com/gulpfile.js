var gulp          = require('gulp');
var path          = require('path');
var browserSync   = require('browser-sync').create();
var $             = require('gulp-load-plugins')(); //自動でプラグインを読み込む
var autoprefixer  = require('autoprefixer');
var babel = require('gulp-babel');

var sassPaths = [
  'node_modules/foundation-sites/scss',
  'node_modules/motion-ui/src',
  'node_modules/ress'
];

function babel_task(){
  return gulp.src('src/js/*.js')
    .pipe(babel(
      {
        "presets": [
          [ "@babel/preset-env", {
            "targets": {
              "browsers": ["last 2 versions", "safari >= 7"]
            }
          } ]
        ],
        "plugins": [
          [
            "@babel/plugin-transform-react-jsx", {
              "pragma": "wp.element.createElement"
            }
          ]
        ]
      }
    ))
    .pipe(gulp.dest('js'));
}

function sass() {
  return gulp.src('src/scss/*.scss')
    .pipe($.plumber({
      errorHandler: $.notify.onError("Error: <%= error.message %>")
    }))
    .pipe($.sourcemaps.init())
    .pipe($.sass({
      includePaths: sassPaths,
      outputStyle: 'compressed' // if css compressed **file size**
    })
    .on('error', $.sass.logError))
    .pipe($.postcss([
      autoprefixer()
    ]))

    .pipe($.sourcemaps.write())
    //.pipe($.sourcemaps.write('dest/tw/css'))
    .pipe(gulp.dest('./css'))
    .pipe(browserSync.stream());
};


function serve() {
  browserSync.init({
    server: "localhost:8080"
  });

  gulp.watch("src/scss/**/*.scss", sass);
  gulp.watch("src/js/**/*.js", babel_task);
}

gulp.task('dev',gulp.series(
  gulp.parallel(sass,babel_task),
  serve
));

gulp.task('build', gulp.parallel(sass, babel_task));

gulp.task('default',gulp.series('dev'));