<?

  $description = get_post_meta($post->ID, "description", true);

  get_header();

  //header と処理が重複しているが、globalよりマシと判断
  [$queried_terms, $queried_term_slugs] = get_queried_recipe_terms();

  //wpで使いやすいように
  $term_array = build_array_from_recipe_terms($queried_terms);
  //リンクにつけるために、逆に、url queryを生成 recipe_genre=cuisine&recipe_season=spring みたいな
  $term_query = build_query_from_recipe_terms($queried_terms);
  //jsonを生成
  $query_json = build_json_from_recipe_terms($queried_terms);
  //自然な日本語を生成
  $query_japanese = build_japanese_from_recipe_terms($queried_terms);

?>
    <div class="l-main-sub">
      <div class="e-main">
        <div class="grid-container">
          <ol class="p-recipe-bc">
            <li class="e-item"><a class="e-link" href="/">トップ</a></li><?
              if(count($queried_terms) != 0){
            ?>
            <li class="e-item"> <a class="e-link m-now" href="<?= home_url('/recipes/?'.$term_query)?>">
                 <?= $query_japanese ?></a></li><?
              }
            ?>
          </ol>
        </div><?
          function get_prev_next_recipe_on_tax_term(){
        
          }
        
          $tax_query = [];
        
          foreach($term_array as $tax => $term){
            array_push($tax_query, [
                'taxonomy' => $tax,
                'field'    => 'slug',
                'terms'    => $term
            ]);
          }
        
          //馬鹿正直に、同じカテゴリのをget_postsでとってくる
          $args = [
            'post_type'=>'recipes',
            'posts_per_page'=>-1,
            'tax_query' => $tax_query,
          ];
        
          $get_posts = new WP_Query;
          $posts_array = $get_posts->query( $args );
        
        
        
          //現在の投稿が何ばんかを見つける
        
          $prev_post = null;
          $next_post = null;
          foreach($posts_array as $index => $a_post){
            if($a_post->ID == $post->ID){
              break;
            }
          }
        
          if($index != 0){
            $prev_post  = $posts_array[$index - 1];
          }
          if($index+1 < count($posts_array)){
            $next_post  = $posts_array[$index + 1];
          }
        
          //その前後をとる
          //SQLがわかればもっとスマートにかけたのだが。
        
        ?>
        
                <nav class="p-recipe-prev-next"><? if($prev_post){ ?>
                  <div class="e-button m-prev"><a class="e-link" href="<?= get_permalink($prev_post->ID).'?'. $term_query ?>"><?=  get_the_post_thumbnail( $prev_post->ID, "thumbnail", ['width'=>100, 'height'=>100, 'class'=>"e-image"]); ?>
                      <div class="e-title"><?= $prev_post->post_title ?></div></a></div><? }else{ ?>
                  <div class="e-button m-prev"></div><? } ?>
                  <? if($next_post){ ?>
                  <div class="e-button m-next"><a class="e-link" href="<?= get_permalink($next_post->ID).'?'. $term_query?>"><?=  get_the_post_thumbnail( $next_post->ID, "thumbnail", ['width'=>100, 'height'=>100, 'class'=>"e-image"]); ?>
                      <div class="e-title"><?= $next_post->post_title ?></div></a></div><? }else{ ?>
                  <div class="e-button m-next"></div><? } ?>
                </nav>
        <div class="grid-container">
          <?php
            while(have_posts()):
              the_post();

              $recipe_terms = get_recipe_terms($id);
          ?>

            <script type="application/ld+json">
              {
                "@context": "https://schema.org/",
                "@type": "Recipe",
                "name": "<?= the_title() ?>",
                "image": [
                  "<?= get_the_post_thumbnail_url( $post->ID, "medium")  ?>",
                  "<?= get_the_post_thumbnail_url( $post->ID, "medium large") ?>",
                  "<?= get_the_post_thumbnail_url( $post->ID, "large")  ?>"
                ],

                "author": {
                  "@type": "Person",
                  "name": "新関さとみ"
                },
                <?
                  $description = get_post_meta($post->ID, "description", true);

                  if($description):
                ?>
                  "description": "<?= $description ?>",
                <? endif;?>
                "keywords": "<? foreach($recipe_terms as $term ){ echo $term->name.','; } ?>",
                "recipeCuisine": "日本 > 山形",
                "recipeCategory": "<?= get_the_terms( $post->ID , 'recipe_genre' )->name ?>"

                <?
                  $recipeIngredient = get_post_meta($post->ID, "recipeIngredient", true);
                  if($recipeIngredient):
                    //前のプロパティが最後になるかもしれないから、こちらでカンマをつけた
                ?>
                  ,"recipeIngredient": [
                    <?
                      $recipeIngredient = get_post_meta($post->ID, "recipeIngredient", true);

                      $array = explode("\n", $recipeIngredient); // とりあえず行に分割
                      $array = array_map('trim', $array); // 各行にtrim()をかける
                      $array = array_filter($array, 'strlen'); // 文字数が0の行を取り除く
                      $ingredient_array = array_values($array); 

                      foreach($ingredient_array as $index => $item):
                    ?>
                      "<?= $item?>"<?= $index!=count($ingredient_array)-1  ? ',' : ''?>

                    <?
                      endforeach;
                    ?>
                  ]
                <? endif; ?>

                <?
                  $recipeInstructions = get_post_meta($post->ID, "recipe_instructions", true);
                  if($recipeInstructions):
                    //前のプロパティが最後になるかもしれないから、こちらでカンマをつけた
                ?>
                  ,"recipeInstructions": [
                    <?


                      $array = explode("\n", $recipeInstructions); // とりあえず行に分割
                      $array = array_map('trim', $array); // 各行にtrim()をかける
                      $array = array_filter($array, 'strlen'); // 文字数が0の行を取り除く
                      $instructions_array = array_values($array); 

                      foreach($instructions_array as $index => $item):
                    ?>
                        {
                          "@type": "HowToStep",
                          "text": "<?= $item ?>"
                        }<?= ($index!=count($instructions_array)-1 ? ',' : '')?>

                    <?
                      endforeach;
                    ?>
                  ]
                <? endif ?>
              }
            </script>
              
          
          <article class="p-recipe entry">
            <header class="p-recipe-header">
              <h1 class="c-h2 m-no-margin"><?php the_title()?></h1>
              <ul class="e-tags"><? foreach($recipe_terms as $term ): ?>
                <li class="e-tag <?= in_array($term->slug, $queried_term_slugs) ? 'm-current':'' ?>"><?= $term->name ?></li><? endforeach; ?>
              </ul>
            </header>
            <div class="e-content entry-content"><? the_content() ?></div>
          </article><?php
            endwhile;
          ?>
          
        </div>
                <nav class="p-recipe-prev-next"><? if($prev_post){ ?>
                  <div class="e-button m-prev"><a class="e-link" href="<?= get_permalink($prev_post->ID).'?'. $term_query ?>"><?=  get_the_post_thumbnail( $prev_post->ID, "thumbnail", ['width'=>100, 'height'=>100, 'class'=>"e-image"]); ?>
                      <div class="e-title"><?= $prev_post->post_title ?></div></a></div><? }else{ ?>
                  <div class="e-button m-prev"></div><? } ?>
                  <? if($next_post){ ?>
                  <div class="e-button m-next"><a class="e-link" href="<?= get_permalink($next_post->ID).'?'. $term_query?>"><?=  get_the_post_thumbnail( $next_post->ID, "thumbnail", ['width'=>100, 'height'=>100, 'class'=>"e-image"]); ?>
                      <div class="e-title"><?= $next_post->post_title ?></div></a></div><? }else{ ?>
                  <div class="e-button m-next"></div><? } ?>
                </nav>
      </div>
      <div class="e-sub">
        <? get_template_part("part-navi", null, ["is_inline"=>true]) ?>
      </div>
      <div class="e-bottom">
        <div class="grid-container fluid" id="banners"> 
          <h2 class="c-h2 m-font-black">さとみの調味料のご購入はこちら <br class="show-for-small-only"><small>(ショッピングサイトを別タブで開きます)</small></h2><?
            $condiment_terms = get_the_terms( $post->ID , 'recipe_condiment' ); 
            $using_shoyu = false;
            $using_tare= false;
            $using_miso= false;
          
            if($condiment_term)
              foreach($condiment_terms as $condiment_term){
                if($condiment_term->slug == 'shoyu'){
                  $using_shoyu = true;
                }
                if($condiment_term->slug == 'tare'){
                  $using_tare = true;
                }
                if($condiment_term->slug == 'miso'){
                  $using_miso = true;
                }
              }
          ?>
          
          <nav class="c-osusume-banners"><a class="<?= $using_shoyu ? 'm-using':'' ?> e-item" href="https://new.niizekisatomi.com/product/mahou-shoyu/" target="_blank">
              <h3 class="c-h3">魔法の醤油</h3>
              <p><img src="<?=get_stylesheet_directory_uri()?>/img/banner_shoyu.jpg"></p></a><a class="<?= $using_miso ? 'm-using':'' ?> e-item" href="https://new.niizekisatomi.com/product/miso/" target="_blank">
              <h3 class="c-h3">18割こうじ味噌</h3>
              <p><img src="<?=get_stylesheet_directory_uri()?>/img/banner_miso.jpg"></p></a><a class="<?= $using_tare ? 'm-using':'' ?> e-item" href="https://new.niizekisatomi.com/product/mahou-tare/" target="_blank">
              <h3 class="c-h3">魔法のたれ</h3>
              <p><img src="<?=get_stylesheet_directory_uri()?>/img/banner_tare.jpg"></p></a><a class="e-item" href="https://tsukemono.info/SHOP/994349/994364/list.html" target="_blank">
              <h3 class="c-h3">レシピ本</h3>
              <p><img src="<?=get_stylesheet_directory_uri()?>/img/banner_recipe_book_2.jpg"></p></a></nav>
        </div>
      </div>
    </div>
<?
  get_footer();
?>